#pragma once
#include <iostream>
#include <string>

#include "olcConsoleGameEngine.h"

using std::wstring;

class Game : public olcConsoleGameEngine
{


public:
	Game()
	{
		this->m_sAppName = L"Fireboy & Watergirl";
	}
	
private:
	wstring sLevel;
	int nLevelWidth;
	int nLevelHeight;

	int nTileWidth;
	int nTileHeight;

protected:
	virtual bool OnUserCreate()
	{
		nLevelWidth = 64;
		nLevelHeight = 16;

		nTileWidth = 16;
		nTileHeight = 16;

		sLevel += L"................................................................";
		sLevel += L"................................................................";
		sLevel += L".......#####....................................................";
		sLevel += L"........###.....................................................";
		sLevel += L".......................########.................................";
		sLevel += L".....##########.......###..............#.#......................";
		sLevel += L"....................###................#.#......................";
		sLevel += L"...................####.........................................";
		sLevel += L"####################################.##############.....########";
		sLevel += L"...................................#.#...............###........";
		sLevel += L"........................############.#............###...........";
		sLevel += L"........................#............#.........###..............";
		sLevel += L"........................#.############......###.................";
		sLevel += L"........................#................###....................";
		sLevel += L"........................#################.......................";
		sLevel += L"................................................................";

		return true;
	}
	virtual bool OnUserUpdate(float fElapsedTime)
	{

		// Drawing the level
		
		int nVisibleTilesX = ScreenWidth() / nTileWidth;
		int nVisibleTilesY = ScreenHeight() / nTileHeight;

		return true;
	}
};

