#include <iostream>
#include <string>
#include "Game.h"

int main()
{
    Game game;
    if(game.ConstructConsole(160, 160, 8, 8))
    {
        game.Start();
    }
}
